package test;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import JUnit.Junit09_Geometria.dto.Geometria;

class GeometriaTest {

	Geometria gm = new Geometria(2);

	@Test
	public void testGetterNom() {
		String resultado = gm.getNom();
		String esperado = "Circulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testGetterId() {
		int resultado = gm.getId();
		int esperado = 2;
		assertEquals(esperado, resultado);
	}
	
	 
	@Test
	public void testSetterNom() {
		gm.setNom("Trianculo");
		String resultado = gm.getNom();
		String esperado = "Trianculo";
		assertEquals(esperado, resultado);
	}

	@Test
	public void testSetterArea() {
		 double areaTriangulo = gm.areatriangulo(2, 5);
		gm.setArea(areaTriangulo); 
		double resultado = gm.getArea();
		double esperado = 5;
		assertEquals(esperado, resultado);
	}

	
	@Test
	public void testFigura() {
		String resultado = gm.figura(3);
		String esperado = "Triangulo";
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testCuadrado() {
		int resultado = gm.areacuadrado(4);
		int esperado = 16;
		assertEquals(esperado, resultado);
	}
	
	
	@Test
	public void testCirculo() {
		double resultado = gm.areaCirculo(2);
		double esperado = 12.5664;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testTriangulo() {
		int resultado = gm.areatriangulo(2, 5);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testRectangulo() {
		int resultado = gm.arearectangulo(2, 5);
		int esperado = 10;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testPentagono() {
		int resultado = gm.areapentagono(2, 5);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testRombo() {
		int resultado = gm.arearombo(2, 5);
		int esperado = 5;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testRomboide() {
		int resultado = gm.arearomboide(2, 3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
	
	@Test
	public void testTrapecio() {
		int resultado = gm.areatrapecio(2, 2, 3);
		int esperado = 6;
		assertEquals(esperado, resultado);
	}
}

